// Copyright 2018 Igor Wojnicki
// This software is distributed under the terms of
// the GNU General Public License version 3

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

function loose(game) {
    window.clearInterval(game.interval);
    game.message="GAME OVER!     Reload the page to restart";
    sound(game,"crash");
    score(game);
}

function win(game) {
    window.clearInterval(game.interval);
    game.message="";
    reset(game);
    game.stage++;
    sound(game,"win");
    countdown(game,3);
}

function showTouchLines(game) {
    game.context.fillStyle = "rgb(0,255,0)";
    game.context.fillRect (game.screen.width/3, 0, 1, game.screen.height);
    game.context.fillRect (game.screen.width*2/3, 0, 1, game.screen.height);
    game.context.fillRect (0,game.screen.height/3, game.screen.width, 1);
    game.context.fillRect (0,game.screen.height*2/3, game.screen.width, 1);
}

function show(game){
    game.context.clearRect(0, 0, game.screen.width, game.screen.height);
    showTouchLines(game);
    for (var row in game.world) {
	for (var column in game.world[row]) {
	    if (row==game.y && column==game.x) { 
		if (game.world[row][column]!=' ') {
		    game.context.fillStyle = "rgb(255,0,0)";
		    game.context.fillRect (column*10, row*10, 10, 10);
		    loose(game);
		}
		else {
		    game.context.fillStyle = "rgb(0,255,0)";
		    game.context.fillRect (column*10, row*10, 10, 10);
		    if (row==0) win(game);
		}
	    } else {
		if (game.world[row][column]!=' '){
		    game.context.fillStyle = "rgb(0,0,0)";
		    game.context.fillRect (column*10, row*10, 10, 10);
		} 
	    }
	}
    }
    game.context.textAlign="left";
    game.context.fillStyle = "rgb(0,0,255)";
    game.context.font="18px Sans";
    game.context.fillText("Stage:"+game.stage+" Pts:"+game.pts+" "+game.message,0, game.screen.height-5);
}

function generate(game, line) {
    var probability;
    for (var i=0; i<game.maxwidth; i++) {
	if (game.world[0][i]==' ') {
	    probability=0.1;
	} else {
	    probability=0.8;
	}
	if (Math.random()<probability) {
	    line[i]='M';
	} else {
	    line[i]=' ';
	}
    }
}

function update(game) {
    var line=new Array(game.maxwidth);
    generate(game, line);
    game.world.unshift(line);
    if (game.world.length>game.maxlength) {
	game.world.pop();
    }
}

function inputUpdate(game) {
    for (var key in game.control.keys) {
	if (game.control.keys[key]==1) // this is a fresh press
	    switch (key) {
	    case 'a': if (game.x>0) game.x--; break;
	    case 'd': if (game.x<game.maxwidth-1) game.x++; break;
	    case 'w': if (game.y>0) game.y--; break;
	    case 's': if (game.y<game.maxlength-1) game.y++; break;
	    }
	game.control.keys[key]=(++game.control.keys[key])%3+1;
//	console.log("game.control.keys = ", game.control.keys);// input (keyboard) repeat every 3 ticks of the game loop
    }
    //    show(game);
    
}

function inputSetup(game) {

    function mouseAndTouchDownHandler(game,pos) {
	if (pos.x<(game.screen.width/3)) game.control.keys['a']=1;
	if (pos.x>(game.screen.width*2/3)) game.control.keys['d']=1;
	if (pos.y<(game.screen.height/3)) game.control.keys['w']=1;
	if (pos.y>(game.screen.height*2/3)) game.control.keys['s']=1;	
    }

    function mouseAndTouchUpHandler(game,pos) {
	if (pos.x<(game.screen.width/3)) delete game.control.keys['a'];
	if (pos.x>(game.screen.width*2/3)) delete game.control.keys['d'];
	if (pos.y<(game.screen.height/3)) delete game.control.keys['w'];
	if (pos.y>(game.screen.height*2/3)) delete game.control.keys['s'];
    }
    
    window.addEventListener('keydown', function(event) {
	if (!game.control.keys[event.key]) game.control.keys[event.key]=1;
    }, false);
    window.addEventListener('keyup', function(event) { 
	delete game.control.keys[event.key];
    }, false);

    // soft buttons

    // game.screen.addEventListener("touchstart", function (e) {
    // 	var rect=game.screen.getBoundingClientRect();
    // 	var pos={x:e.touches[0].clientX-rect.left, x:e.touches[0].clientY-rect.top};
    // 	if (pos.x<(game.width/3)) game.control.keys['a']=1;
    // });

    game.screen.addEventListener("mousedown", function (e) {
	var rect=game.screen.getBoundingClientRect();
	var pos={x:e.clientX-rect.left, y:e.clientY-rect.top};
	mouseAndTouchDownHandler(game,pos);
    });

    game.screen.addEventListener("mouseup", function (e) {
	var rect=game.screen.getBoundingClientRect();
	var pos={x:e.clientX-rect.left, y:e.clientY-rect.top};
	mouseAndTouchUpHandler(game,pos);
    });

    game.screen.addEventListener("touchstart", function (e) {
	var rect=game.screen.getBoundingClientRect();
	var pos={x:e.changedTouches[0].clientX-rect.left, y:e.changedTouches[0].clientY-rect.top};
	mouseAndTouchDownHandler(game,pos);
    });

    game.screen.addEventListener("touchend", function (e) {
	var rect=game.screen.getBoundingClientRect();
	var pos={x:e.changedTouches[0].clientX-rect.left, y:e.changedTouches[0].clientY-rect.top};
	mouseAndTouchUpHandler(game,pos);
    });

    
    // var buttons=['a','s','d','w'];
    // for (var b in buttons) {
    // 	document.getElementById(buttons[b]).ontouchstart=function () {
    // 	    game.control.keys[this.value]=true;
    // 	};
    // 	document.getElementById(buttons[b]).ontouchend=function () {
    // 	    delete game.control.keys[this.value];
    // 	};
    // 	document.getElementById(buttons[b]).onmousedown=function () {
    // 	    game.control.keys[this.value]=true;
    // 	};
    // 	document.getElementById(buttons[b]).onmouseup=function () {
    // 	    delete game.control.keys[this.value];
    // 	};
    // }
    // window.oncontextmenu = function(event) { // stop context menus
    // 	event.preventDefault();
    // 	event.stopPropagation();
    // 	return false;
    // };
    // window.selectstart = function(event) { // stop text selection
    // 	event.preventDefault();
    // 	event.stopPropagation();
    // 	return false;
    // };    
}

function reset(game) {
    for(var i = 0; i < game.maxlength; i++) {
	for(var j = 0; j < game.maxwidth; j++) {
	    game.world[i][j]=' ';
	}
    }    
    game.x=Math.round(game.maxwidth/2);
    game.y=game.maxlength-1;
}

function init(elementId) {
    var game=new Object();
    game.world=new Array();
    game.screen=document.getElementById(elementId);
    game.maxlength=25;
    game.maxwidth=60;
    game.x=Math.round(game.maxwidth/2);
    game.y=game.maxlength-1;
    game.stage=1;
    game.time=0;
    game.pts=0;
    game.message="";
    game.control=new Object();
    game.control.keys=new Object();
    game.leaderboard=JSON.parse(window.localStorage.getItem("topdropLeaderboard"));
    if (game.leaderboard==null)
	game.leaderboard=new Array();
    for(var i = 0; i < game.maxlength; i++) {
	var line=new Array(game.maxwidth);
	for(var j = 0; j < game.maxwidth; j++) {
	    line[j]=' ';
	}
	game.world[i]=line;
    }
    inputSetup(game);
    game.context = game.screen.getContext("2d");
    game.screen.height=game.maxlength*10;
    game.screen.width=game.maxwidth*10;
    // create web audio api context
    game.audioCtx = new (window.AudioContext || window.webkitAudioContext)();
    return game;
}

function gameLoop(game) {
    inputUpdate(game);
    var speed=21-(3*game.stage);
    if (game.time % (speed?speed:1) == 0) {
//    if (game.time % 15 == 0) {
	sound(game,'tick');
	update(game);
	game.pts++;
    }
    game.time++;
    show(game);
}

function run(game) {
    game.interval=window.setInterval(gameLoop,33,game);
}

function countdown(game,value) {
    game.message="                          GET READY IN: "+value;
//    show(game);
    title(game);
    if (value==0) {
	game.message="";
	sound(game,"beeph");
	run(game);
    } else {
	sound(game,"beep");
	window.setTimeout(countdown,1000,game,--value);
    }
}


function sound(game,name) {
    // create Oscillator node
    game.oscillator = game.audioCtx.createOscillator();
    switch (name) {
    case 'tick':
	game.oscillator.frequency.setValueAtTime(110, game.audioCtx.currentTime); // value in hertz
	game.oscillator.connect(game.audioCtx.destination);
	game.oscillator.type="sine";
	game.oscillator.start();
	game.oscillator.stop(game.audioCtx.currentTime + 0.02);
	break;
    case 'crash':
	game.oscillator.frequency.setValueAtTime(120, game.audioCtx.currentTime); // value in hertz
	game.oscillator.connect(game.audioCtx.destination);
	game.oscillator.type="sawtooth";
	game.oscillator.start();
	game.oscillator.stop(game.audioCtx.currentTime + 0.3);
	break;
    case 'win':
	game.oscillator.frequency.setValueAtTime(220, game.audioCtx.currentTime); // value in hertz
	game.oscillator.connect(game.audioCtx.destination);
	game.oscillator.type="sine";
	game.oscillator.start();
	game.oscillator.frequency.exponentialRampToValueAtTime(2400, game.audioCtx.currentTime+0.2);
	game.oscillator.stop(game.audioCtx.currentTime + 0.2);
	break;
    case 'beep':
	game.oscillator.frequency.setValueAtTime(440, game.audioCtx.currentTime); // value in hertz
	game.oscillator.connect(game.audioCtx.destination);
	game.oscillator.type="sine";
	game.oscillator.start();
	game.oscillator.stop(game.audioCtx.currentTime + 0.1);
	break;
    case 'beeph':
	game.oscillator.frequency.setValueAtTime(880, game.audioCtx.currentTime); // value in hertz
	game.oscillator.connect(game.audioCtx.destination);
	game.oscillator.type="sine";
	game.oscillator.start();
	game.oscillator.stop(game.audioCtx.currentTime + 0.1);
	break;
    }
}

function title(game) {
    game.context.clearRect(0, 0, game.screen.width, game.screen.height);
    game.context.fillStyle = "rgb(0,0,0)";
    game.context.font="48px monospace";
    game.context.textAlign="center";
    game.context.fillText("TOP DROP",game.screen.width/2, 50);
    game.context.fillStyle = "rgb(255,64,64)";
    game.context.font="28px monospace";
    game.context.fillText("leaderboard",game.screen.width/2, 80);
    var vertPos=80;
    game.context.fillStyle = "rgb(255,32,32)";
    game.context.font="18px monospace";
    for (var item in game.leaderboard) {
	vertPos+=18;
	game.context.fillText(game.leaderboard[item],game.screen.width/2, vertPos);	
    }
    game.context.textAlign="left";
    game.context.fillStyle = "rgb(0,0,255)";
    game.context.font="18px Sans";
    game.context.fillText(game.message,0, game.screen.height-5);
}

function score(game) {
    var item;
    var added=false;
    for (item in game.leaderboard) {
	if (game.pts>game.leaderboard[item]) {
	    game.leaderboard.splice(item,0,game.pts);
	    added=true;
	    break;
	}
    }
    if (!added) game.leaderboard.push(game.pts);
    if (game.leaderboard.length > 6) game.leaderboard.shift();
    window.localStorage.setItem("topdropLeaderboard",JSON.stringify(game.leaderboard));
}

var g=init("screen");
countdown(g,3);











