# How to run?

Welcome to TOP DROP, a retro inspired web browser game.

To run: download and just open index.html in a browser, or try it out online: http://igorwojnicki.gitlab.io/top_drop/

# How to play?

* Move up, down, left, right with w, s, a, d.
* Don't hit the black blocks - you'll die.
* Longer you stay alive more points you get.
* Get to the top for the next stage.
* Next stage is faster - think twice if you want to go there...


